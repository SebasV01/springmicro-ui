$(document).ready(function () {
    var id = sessionStorage.getItem("id");  
	var token = sessionStorage.getItem("k");

    if(id > 0){
        $.ajax({
            type: "GET",
            url: "https://sebas-zuul-sp.herokuapp.com/micro-client/persona/leer/" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
			headers: {
				"Authorization": "Bearer " + token
				},
            success: function (data) {
                var nombre_completo = data.nombre_completo;
                var pais = data.pais;
                var bio = data.bio;
                var foto = data.fotoURL;

                var headerName = $("#headerName");
                var lblPais = $("#lblPais");
                var lblBio = $("#lblBio");
                $("#lblFoto").attr("src", foto);

                headerName.html(nombre_completo);
                lblPais.html(pais);
                lblBio.html(bio);
            },
            error: function (XMLhttpRequest, textStatus, errorThrown) {
                toastr.error("Request: " + XMLhttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            }
        });

        $.ajax({
            type: "GET",
            url: "https://sebas-zuul-sp.herokuapp.com/micro-client/habilidad/listar/" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
			headers: {
				"Authorization": "Bearer " + token
				},
            success: function (data) {
                for (var i = 0; i < data.length; i++) {  
                    var exp = data[i].experiencia;
                    var span;

                    if(exp === "A"){
                        span = $('<span />').addClass('label label-success').html(data[i].nombre); 
                    }else if(exp === "B"){
                        span = $('<span />').addClass('label label-info').html(data[i].nombre); 
                    }else if(exp === "R"){
                        span = $('<span />').addClass('label label-warning').html(data[i].nombre); 
                    }

                    $("#lstHabilidades").append(span);
                }
            },
            error: function (XMLhttpRequest, textStatus, errorThrown) {
                toastr.error("Request: " + XMLhttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            }
        });
    }else{
        window.location.href = "./index.html";
    }
	
	    $("#btnCerrar").click(function () {
        sessionStorage.clear();
        window.location.href = "./index.html"
    });
	
	 $(document).on('click', "#btnEnviar", function (e) {        
        $.ajax({
            type: "GET",
            url: "https://sebas-zuul-sp.herokuapp.com/micro-client/node-client/getURL",
            async: false,
            dataType: "text",
			headers: {
				"Authorization": "Bearer " + token
				},
            success: function (data) {
                if (data == 'no_data') {
                    toastr.warning("No existe una constancia generada");
                } else {
                    window.open(
                        data
                    );
                }
            },

            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            }
        });
    });
});
function showPassword() {
    
    var key_attr = $('#key').attr('type');

    if (key_attr != 'text') {

        $('.checkbox').addClass('show');
        $('#key').attr('type', 'text');

    } else {

        $('.checkbox').removeClass('show');
        $('#key').attr('type', 'password');

    }
}

$(document).ready(function () {
    $("#btn-login").click(function () {
		let usuario = document.getElementById("txtUsuario").value;   
		let clave = document.getElementById("txtClave").value;   
        var correo = document.getElementById("email").value; 
	
		var input = correo;
		
		let body = {
				"grant_type":"password",
				"client_id":"sebas",
				"username": usuario,
				"password": clave				
				}
				
		let encoded = window.btoa("sebas:sebassecret");	
		console.log(encoded);
	
		$.ajax({
					type: "POST",
					url:  "https://sebas-zuul-sp.herokuapp.com/uaa/oauth/token",//"http://localhost:8099/uaa/oauth/token",
					contentType: "application/x-www-form-urlencoded",
					dataType: "json",									
					headers: {
						"Authorization": "Basic " + encoded					
					},
					data: body,
					success: function (data) {						
						console.log(data);
						token = data.access_token;
						sessionStorage.setItem("k", token);	

						$.ajax({
							type: "POST",
							url:  "https://sebas-zuul-sp.herokuapp.com/micro-client/persona/leerCorreo?access_token=" + token, //"https://mito-zuul-sp.herokuapp.com/micro-client/persona/leerCorreo/" + correo,
							contentType: "application/json; charset=utf-8",
							data : JSON.stringify(input),
							dataType: "json",				
							success: function (data) {                
							
								if(data > 0){
									sessionStorage.setItem("id", data);
									window.location.href = "./principal.html";
								}else{
									toastr.warning("Credenciales incorrectas");
								}
							},									
						});
					},
	
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						toastr.error("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
					}
				});	            
        });

});

